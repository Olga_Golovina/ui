package registration;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static registration.BasePage.clickButton;

public class step2 {
    public WebDriver driver;

    public step2(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    @FindBy(css = ".toggle")
    public static WebElement toggle;

    @FindBy(css = ".btn-fill")
    public static WebElement toggle1;

    @FindBy(id = "process")
    public static WebElement confirmar;

    public static String number;


    @Step("Открываем автокомплит и заполняем все данные")
    public static void Autocmplete(WebDriver driver) {

        clickButton(driver, toggle);
        clickButton(driver, toggle1);
    }


    @Step("Сохраняем номер email")
    public static String NumberEmail(WebDriver driver) {

        number = driver.findElement(By.name("email")).getAttribute("value");
        return number;

    }


}


