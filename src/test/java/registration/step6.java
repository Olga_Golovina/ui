package registration;

import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static registration.BasePage.clickButton;
import static registration.BasePage.enterField;

public class step6 {
    public WebDriver driver;

    public step6(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }


    @Step("Открытие нового окна")
    public static void openInNewWindow(WebDriver tempDriver, String url) {
        String name = "admin";
        ((JavascriptExecutor) tempDriver)
                .executeScript("window.open(arguments[0],\"" + name + "\")", url);
    }


    @FindBy(name = "login")
    public static WebElement loginAdmin;

    @FindBy(xpath = "//*[@id=\"login_eplouyee\"]/div[2]/div/input")
    public static WebElement passwordAdmin;

    @FindBy(xpath = "//*[@id=\"login_eplouyee\"]/div[3]/div/button")
    public static WebElement login;

    @Step("Авторизация в админке")
    public static void AutorizationAdmin(WebDriver driver) {

        driver.switchTo().window("admin");
        enterField(driver, loginAdmin, "admin");
        enterField(driver, passwordAdmin, "123456");
        clickButton(driver, login);

    }


}
