package registration;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;


public class step8 {
    public WebDriver driver;

    public step8(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "//*[@id=\"app\"]/div[1]/main/div/div/div[2]/div/div/div/div/div/div/div/div/div/div/div[1]/h1")
    public static WebElement Rechazado;

    @FindBy(xpath = "/html/body/div/div[1]/header/div/div/div[2]/div/div/div/div[3]/button")
    public static WebElement Salir;

    @Step("Проверяем, отображается ли элемент")
    public static void checkElementDisplayed(WebElement element) {

        Assert.assertTrue(element.isDisplayed());
    }

    @Step("Обновляем страницу ЛК")
    public static void cabinet(WebDriver driver) throws InterruptedException {

        driver.navigate().refresh();
        Thread.sleep(5000);
        driver.navigate().refresh();
        registration.BasePage.wait(driver, Rechazado);
    }
}
