package registration;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class step1 {
    public WebDriver driver;

    public step1(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }


    @FindBy(id = "register_button")
    public static WebElement ConseguirDinero;


    @Step("Открываем необходимую страницу")
    public static void openLoginPage(WebDriver driver) {

        driver.get("https://spa-master.test-es.hlp.systems/");

    }

}

