package registration;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import static registration.BasePage.clickButton;
import static registration.BasePage.enterField;

public class step5 {
    public WebDriver driver;

    public step5(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    @FindBy(xpath = "//*[@id=\"root\"]/div/div/main/section[3]/div[2]/button")
    public static WebElement Acepatar;

    @FindBy(xpath = "//*[@id=\"banks-grid\"]/button[1]")
    public static WebElement Copito;

    @FindBy(xpath = "//*[@id=\"root\"]/div/div/main/div[1]/nav/div[1]/button")
    public static WebElement Particulares;

    @FindBy(xpath = "//*[@id=\"root\"]/div/div/main/form/input")
    public static WebElement seach;

    @FindBy(xpath = "//*[@id=\"id-username-0\"]")
    public static WebElement username;

    @FindBy(xpath = "//*[@id=\"id-password-0\"]")
    public static WebElement password;

    @FindBy(xpath = "//*[@id=\"root\"]/div/div/main/section/form/section/section/label/span")
    public static WebElement check;

    @FindBy(xpath = "//*[@id=\"root\"]/div/div/main/section/form/div[3]/button")
    public static WebElement Acceder;

    @FindBy(xpath = "//*[@id=\"target-step\"]/form/div/div[2]/button")
    public static WebElement Siguiente;

    @FindBy(xpath = "//*[@id=\"give-consents-step\"]/form/div/div[1]/div/div[1]/div/label/input")
    public static WebElement chekkbox1;

    @FindBy(xpath = "//*[@id=\"give-consents-step\"]/form/div/div[2]/button[1]/span")
    public static WebElement Siguiente2;

    @FindBy(xpath = "//*[@id=\"enter-credential-step\"]/form/div[1]/div[1]/div/input")
    public static WebElement Identificación;

    @FindBy(xpath = "//*[@id=\"enter-credential-step\"]/form/div[1]/div[2]/button[1]")
    public static WebElement Siguiente3;

    @FindBy(name = "credentials[0]")
    public static WebElement secreto;

    @FindBy(xpath = "//*[@id=\"enter-credential-step\"]/form/div[1]/div[2]/button[1]/span")
    public static WebElement Siguiente4;

    @Step("Проходим регистрацию в iframe (unnax/kontomatik)")
    public static void Iframe(WebDriver driver) throws InterruptedException {

        String url = driver.getCurrentUrl();
        if (url.contains("unnax")) {


            //ожидаем загрузку frame
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("unnax-script")));

            //переключаемся на frame
            driver.switchTo().frame("unnax-script");

            //проходим регистрацию в frame unnax с ожиданием на каждом шаге
            registration.BasePage.wait(driver, seach);
            enterField(driver, seach, "Copito");
            registration.BasePage.wait(driver, Copito);
            clickButton(driver, Copito);
            clickButton(driver, Particulares);
            registration.BasePage.wait(driver, username);
            enterField(driver, username, "pr3627_c");
            enterField(driver, password, "unnax4321");
            clickButton(driver, check);
            clickButton(driver, Acceder);
        } else {
            WebDriverWait wait = new WebDriverWait(driver, 300);
            wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("kontomatik")));

            //переключаемся на frame
            // driver.switchTo().frame("kontomatik");
            driver.switchTo().frame(0);

            //проходим регистрацию в frame kontomatik с ожиданием на каждом шаге
            registration.BasePage.wait(driver, Siguiente);
            clickButton(driver, Siguiente);
            registration.BasePage.wait(driver, chekkbox1);
            clickButton(driver, chekkbox1);
            clickButton(driver, Siguiente2);
            registration.BasePage.wait(driver, Identificación);
            enterField(driver, Identificación, "test");
            clickButton(driver, Siguiente3);
            registration.BasePage.wait(driver, secreto);
            Thread.sleep(2000);
            enterField(driver, secreto, "Test123");
            clickButton(driver, Siguiente4);

        }

    }


}
