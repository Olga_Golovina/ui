package registration;

import io.qameta.allure.Step;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Test;
import static registration.step2.*;
import static registration.BasePage.clickButton;
import static registration.BasePage.enterField;


public class step7Admin {
    public WebDriver driver;

    private Test newTest;

    public step7Admin(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//*[@id=\"client_grid\"]/thead/tr[3]/td[5]/input")
    public static WebElement inputemail;

    @FindBy(xpath = "//*[@id=\"client_grid\"]/tbody/tr/td[3]/a")
    public static WebElement client;

    @FindBy(xpath = "/html/body/div/div[1]/section[2]/div/div[3]/div[3]/div[2]/form/div/button")
    public static WebElement ban;

    @FindBy(xpath = "//*[@id=\"ban_reason\"]")
    public static WebElement banReason;

    @FindBy(xpath = "//*[@id=\"ban_reason\"]/option[3]")
    public static WebElement Fraud;

    @FindBy(xpath = "/html/body/div/header/a/span[2]/img")
    public static WebElement logo;

    @Step("Ищем нужного клиента и блокируем его")
    public static void ClientBan(WebDriver driver) {
        registration.BasePage.wait(driver, inputemail);
        enterField(driver, inputemail, number);
        inputemail.sendKeys(Keys.ENTER);
        registration.BasePage.wait(driver, client);
        clickButton(driver, client);
        registration.BasePage.wait(driver, logo);
        Select selector = new Select(banReason);
        selector.selectByValue("3");
        clickButton(driver, ban);
        driver.close();
    }

}
