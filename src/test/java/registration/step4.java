package registration;

import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static registration.BasePage.clickButton;

public class step4 {
    public WebDriver driver;

    public step4(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }


    @FindBy(css = ".btn-fill")
    public static WebElement toggle2;

    @FindBy(id = "process")
    public static WebElement enviarDatos;

    @Step("Заполняем все данные автокомплитом")
    public static void Autocmplete1(WebDriver driver) throws InterruptedException {

        Thread.sleep(1000);
        clickButton(driver, toggle2);

        registration.BasePage.wait(driver, enviarDatos);
        Thread.sleep(1000);

        clickButton(driver, enviarDatos);
    }


}
