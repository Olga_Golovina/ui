import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import registration.*;

import javax.swing.text.html.parser.Element;
import java.util.concurrent.TimeUnit;

import static registration.BasePage.*;
import static registration.step1.*;
import static registration.step2.*;
import static registration.step3.*;
import static registration.step4.*;
import static registration.step5.*;
import static registration.step6.*;
import static registration.step7Admin.*;
import static registration.step8.*;


public class Test {
    public WebDriver driver;
    public registration.BasePage BasePage;
    private step1 newstep1;
    private step2 newstep2;
    private step3 newstep3;
    private step4 newstep4;
    private step5 newstep5;
    private step6 newstep6;
    private step7Admin newstep7Admin;
    private step8 newstep8;

    @BeforeMethod
    public void setup() {
        BasePage = new BasePage();
        driver = BasePage.initialize_driver();
        newstep1 = new step1(driver);


    }

    @org.testng.annotations.Test(description = "Корректная регистрация")
    public void testCorrect() throws InterruptedException {

        //Открываем нужную страницу
        openLoginPage(driver);

        String clients = driver.getWindowHandle();
        registration.BasePage.wait(driver, ConseguirDinero);

        //кликаем на Conseguir dinero
        clickButton(driver, ConseguirDinero);

        newstep2 = new step2(driver);

        Autocmplete(driver);
        NumberEmail(driver);

        //Нажимаем confirmar
        clickButton(driver, confirmar);


        newstep3 = new step3(driver);

        Thread.sleep(1000);

        //Нажимаем     confirmar numero
        clickButton(driver, confirmarnymero);

        newstep4 = new step4(driver);

        Autocmplete1(driver);

        newstep5 = new step5(driver);
        Iframe(driver);

        //открываем второй браузер

        newstep6 = new step6(driver);

        openInNewWindow(driver, "https://api.test-es.hlp.systems/admin/client");
        AutorizationAdmin(driver);

        newstep7Admin = new step7Admin(driver);

        ClientBan(driver);

        driver.switchTo().window(clients);

        newstep8 = new step8(driver);
        cabinet(driver);

        checkElementDisplayed(Rechazado);


    }

    @AfterMethod
    public void teardown(ITestResult result) {
        makeScreenOnTestFail(result);
        if (driver != null) {
            driver.quit();

        }
    }
}
